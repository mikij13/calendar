<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 19-Jul-19
 * Time: 01:09
 */
?>
<div class="row">
    <div class="col-3">
        <a href="index.php?month=prev" class="btn btn-primary">Previous</a>
    </div>
    <div class="col-6">
        <h3><?php echo date('F Y', $_SESSION['date'])?></h3>
    </div>
    <div class="col-3">
        <a href="index.php?month=next" class="btn btn-primary">Next</a>
    </div>
</div>

