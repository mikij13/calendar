<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 19-Jul-19
 * Time: 01:09
 */
?>
<div class="row">
    <div class="col-3">
        <?php echo "<a href='dailyEvents.php?day=" . (date("j",$_SESSION['date'])-1) . "&month=" . date("m",$_SESSION['date']) . "&year=" . date("Y",$_SESSION['date']) . "' class='btn btn-primary'>Previous</a>" ?>
    </div>
    <div class="col-6">
        <h3><?php echo date('L F jS Y', $_SESSION['date'])?></h3>
    </div>
    <div class="col-3">
        <?php echo "<a href='dailyEvents.php?day=" . (date("j",$_SESSION['date'])+1) . "&month=" . date("m",$_SESSION['date']) . "&year=" . date("Y",$_SESSION['date']) . "' class='btn btn-primary'>Previous</a>" ?>
    </div>
</div>

