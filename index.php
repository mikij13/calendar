<?php
    /**
     * Created by PhpStorm.
     * User: Michal
     * Date: 19-Jul-19
     * Time: 01:09
     */
    session_start();
    if (!isset($_SESSION['date']))
    {
        $_SESSION['date'] = new DateTime();
    }
    if (isset($_GET['month']))
    {
        switch ($_GET['month']){
            case "next": $_SESSION['date'] = mktime(date('h',$_SESSION['date']), date('i',$_SESSION['date']),date('s',$_SESSION['date']),date('n',$_SESSION['date'])+1,date('j',$_SESSION['date']), date('Y',$_SESSION['date'])); break;
            case "prev": $_SESSION['date'] = mktime(date('h',$_SESSION['date']), date('i',$_SESSION['date']),date('s',$_SESSION['date']),date('n',$_SESSION['date'])-1,date('j',$_SESSION['date']), date('Y',$_SESSION['date'])); break;
        }
    }
?>
<!DOCTYPE html>

<html  lang="en">
    <?php
        require_once (__DIR__.'partials/head.php')
    ?>

    <body>
    <?php
        require_once (__DIR__ . 'partials/monthMenu.php');
        require_once (__DIR__.'Classes/Calendar.php');
        Calendar::getMonth($_SESSION['date']);
        require_once (__DIR__.'partials/footer-scripts.php');
    ?>
    </body>

</html>


