<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 18-Jul-19
 * Time: 19:46
 */

require_once (__DIR__ . 'Database.php');

class Event
{
    private $id;
    private $name;
    private $description;
    private $start;
    private $end;
    private $daily;
    private $weekly;
    private $monthly;
    private $yearly;
    private $wholeDay;
    private $userId;

    public function __construct($pid, $pname, $pdescription, $pstart, $pend, $pdaily, $pweekly, $pmonthly, $pyearly, $pwholeDay, $puserId)
    {
        $this->id = $pid;
        $this->name = $pname;
        $this->description = $pdescription;
        $this->start = $pstart;
        $this->end = $pend;
        $this->daily = $pdaily;
        $this->weekly = $pweekly;
        $this->monthly = $pmonthly;
        $this->yearly = $pyearly;
        $this->wholeDay = $pwholeDay;
        $this->userId = $puserId;
    }

    public function getDescription()
    {
        $repeat = "One time event.";
        if($this->daily){$repeat = "Daily.";}
        if($this->weekly){$repeat = "Weekly.";}
        if($this->monthly){$repeat = "Monthly.";}
        if($this->yearly){$repeat = "Yearly.";}
        $text = "<div class='card'>
                    <div class='card-body'>
                        <h5 class='card-title'>" . $this->name . "</h5>
                        <p>". $this->description ."</p>
                        <p>Start: ". date('H:i d-m-Y', $this->start) ."</p>
                        <p>End: ". date ('H:i d-m-Y', $this->end) ."</p>
                        <p>Repeat: ". $repeat ."</p>
                    </div>
                 </div>";
        return $text;
    }



    public static function create($pname, $pdesciption, $pstart, $pend, $prepeat, $pwholeDay, $puser){
        try
        {
            $daily = false;
            $weekly = false;
            $monthly = false;
            $yearly = false;

            switch($prepeat){
                case "daily": $daily = true; break;
                case "weekly": $weekly = true; break;
                case "monthly": $monthly = true; break;
                case "yearly": $yearly = true; break;
            }

            $sql = "INSERT INTO events(name, description, start, end, daily, weekly, monthly, yearly, wholeday, user_id) VALUES(:name, :description, :start, :end, :daily, :weekly, :monthly, :yearly, :wholeday, :user)";

            $database = Database::getInstance();
            $db = $database->getConnection()->prepare($sql);

            $db->bindparam(":name", $pname);
            $db->bindparam(":description", $pdesciption);
            $db->bindparam(":start", $pstart);
            $db->bindparam(":end", $pend);
            $db->bindparam(":daily", $daily);
            $db->bindparam(":weekly", $weekly);
            $db->bindparam(":monthly", $monthly);
            $db->bindparam(":yearly", $yearly);
            $db->bindparam(":wholeday", $pwholeDay);
            $db->bindparam(":user", $puser);


            $db->execute();

            return true;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
            return false;
        }
    }

    public static function edit($pid, $pname, $pdesciption, $pstart, $pend, $prepeat, $pwholeDay, $puser){
        try
        {
            $daily = false;
            $weekly = false;
            $monthly = false;
            $yearly = false;

            switch($prepeat){
                case "daily": $daily = true; break;
                case "weekly": $weekly = true; break;
                case "monthly": $monthly = true; break;
                case "yearly": $yearly = true; break;
            }

            $sql = "UPDATE events SET name = :name, description = :description, start = :start, end = :end, daily = :daily, weekly = :weekly, monthly = :monthly, yearly = :yearly wholeday = :wholeday, user_id = :user) WHERE id = :id";

            $database = Database::getInstance();
            $db = $database->getConnection()->prepare($sql);

            $db->bindparam(":name", $pname);
            $db->bindparam(":description", $pdesciption);
            $db->bindparam(":start", $pstart);
            $db->bindparam(":end", $pend);
            $db->bindparam(":daily", $daily);
            $db->bindparam(":weekly", $weekly);
            $db->bindparam(":monthly", $monthly);
            $db->bindparam(":yearly", $yearly);
            $db->bindparam(":wholeday", $pwholeDay);
            $db->bindparam(":user", $puser);
            $db->bindparam(":id", $pid);


            $db->execute();

            return true;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
            return false;
        }
    }

    public static function delete($pid) {
        try
        {
            $sql = "DELETE FROM evemts WHERE id = :id";

            $database = Database::getInstance();
            $db = $database->getConnection()->prepare($sql);

            $db->bindparam(":id", $pid);

            $db->execute();

            return true;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
            return false;
        }
    }

    public static function dailyCount($pdate){
        try{
            $sql = "SELECT COUNT(*) as total FROM events WHERE (start > :dayStart AND start < :dayEnd) 
                      OR (end > :dayStart AND end < :dayEnd)  OR  (daily = true)  
                      OR (weekly = true AND (WEEKDAY(start) = :weekday OR WEEKDAY(end) = :weekday)) 
                      OR (monthly = true AND (DAYOFMONTH(start) = :monthday OR DAYOFMONTH(end) = :monthday))
                      OR (yearly = true AND ((DAYOFMONTH(start) = :monthday AND MONTH(start) = :month) OR (DAYOFMONTH(end) = :monthday AND MONTH(end) = :month)))
                    )";
            $database = Database::getInstance();
            $db = $database->getConnection()->prepare($sql);

            $db->bindparam(":dayStart", mktime(0,0,0,date('m',$pdate), date('d', $pdate), date('year', $pdate)));
            $db->bindparam(":dayEnd", mktime(0,0,0,date('m',$pdate), date('d', $pdate)+1, date('year', $pdate)));
            $db->bindparam(":weekday", date('w', $pdate));
            $db->bindparam(":monthday", date('d', $pdate));
            $db->bindparam(":month", date('m', $pdate));

            $db->execute();

            $result = $db->fetch(PDO::FETCH_ASSOC);
            return $result['total'];

        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
            return 0;
        }
    }

    public static function dayEvents($pdate){
        try{
            $sql = "SELECT * FROM events WHERE (start > :dayStart AND start < :dayEnd) 
                      OR (end > :dayStart AND end < :dayEnd)  OR  (daily = true)  
                      OR (weekly = true AND (WEEKDAY(start) = :weekday OR WEEKDAY(end) = :weekday)) 
                      OR (monthly = true AND (DAYOFMONTH(start) = :monthday OR DAYOFMONTH(end) = :monthday))
                      OR (yearly = true AND ((DAYOFMONTH(start) = :monthday AND MONTH(start) = :month) OR (DAYOFMONTH(end) = :monthday AND MONTH(end) = :month)))
                    )";
            $database = Database::getInstance();
            $db = $database->getConnection()->prepare($sql);

            $db->bindparam(":dayStart", mktime(0,0,0,date('m',$pdate), date('d', $pdate), date('year', $pdate)));
            $db->bindparam(":dayEnd", mktime(0,0,0,date('m',$pdate), date('d', $pdate)+1, date('year', $pdate)));
            $db->bindparam(":weekday", date('w', $pdate));
            $db->bindparam(":monthday", date('d', $pdate));
            $db->bindparam(":month", date('m', $pdate));

            $db->execute();

            $count= $db->rowCount();
            $events = array();

            for($i = 0; $i<$count; $i++){
                $row=$db->fetch(PDO::FETCH_ASSOC);
                $result[] = new Event($row['id'], $row['name'], $row['description'], $row['start'], $row['end'], $row['daily'], $row['weekly'], $row['monthly'], $row['yearly'], $row['wholeday'], $row['user_id']);
            }

            return $events;

        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
            return null;
        }
    }

}