<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 18-Jul-19
 * Time: 20:28
 */
require_once (__DIR__ . 'Database.php');

class User
{
    private $id;
    private $username;
    private $password;

    public static function create($pusername, $ppassword){
        try
        {

            $sql = "INSERT INTO users(username, password) VALUES(:username, :password)";

            $database = Database::getInstance();
            $db = $database->getConnection()->prepare($sql);

            $db->bindparam(":username", $pusername);
            $db->bindparam(":password", $ppassword);

            $db->execute();

            return true;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
            return false;
        }
    }

    public static function login($pusername, $ppassword){
        try{
            $sql = "SELECT * FROM users WHERE username = :username";

            $database = Database::getInstance();
            $db = $database->getConnection()->prepare($sql);

            $db->bindparam(":username", $pusername);

            $db->execute();

            $count = $db->rowCount();

            if($count == 1){
                $result = $db->fetch(PDO::FETCH_ASSOC);
                if($result['password'] == $ppassword){
                    return $result['id'];
                } else {
                    return "password";
                }
            } else {
                return "username";
            }

        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
            return "database";
        }
    }
}