<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 18-Jul-19
 * Time: 22:17
 */

class Calendar
{
    public static function getMonth($pdate = null){
        if($pdate == null){
            $pdate = new DateTime();
        }
        $text = "";
        for ($i = 1; $i<= date('t',$pdate); $i++){
            $currentDay = mktime(0,0,0, date('m', $pdate), $i, date('Y', $pdate) );
            $text .= "  <div class='card'>
                            <div class='card-body'>
                                <h5 class='card-title'>" . $i . "</h5>
                                <p>". Event::dailyCount($currentDay) ." events</p>
                                <a href='dailyEvents.php?day=" . date("j",$currentDay) . "&month=" . date("m",$currentDay) . "&year=" . date("Y",$currentDay) . "' class='btn btn-primary'>Show More</a>
                            </div>
                         </div>";
        }
    }

    public static function getDay($pdate){
        $text = "";
        $events = Event::dayEvents($pdate);
        foreach ($events as $event) {
            $text .= $event->getDescription();
        }
    }
}