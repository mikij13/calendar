<?php
/**
 * Created by PhpStorm.
 * User: Michal
 * Date: 19-Jul-19
 * Time: 12:41
 */
session_start();
if (!isset($_SESSION['date']))
{
    $_SESSION['date'] = new DateTime();
}
if (isset($_GET['day']) & isset($_GET['month']) & isset($_GET['year']))
{
    $_SESSION['date'] = mktime(0,0,0, $_GET['month'], $_GET['day'], $_GET['year']);
}
?>
<!DOCTYPE html>

<html  lang="en">
    <?php
        require_once (__DIR__.'partials/head.php')
    ?>

    <body>
    <?php
        require_once (__DIR__ . 'partials/dayMenu.php');
        require_once (__DIR__.'Classes/Calendar.php');
        Calendar::getDay($_SESSION['date']);
        require_once (__DIR__.'partials/footer-scripts.php');
    ?>
</body>

</html>


